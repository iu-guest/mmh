#
# INSTALL -- installation instructions
#

--------------
Installing mmh
--------------
Please read all of the following instructions before you begin
building mmh.

Mmh can be built and installed on POSIX-compliant systems, using
an ANSI C compiler. (Check out docs/MACHINES for shortcomings of
operating systems.)

0) If you have obtained mmh by checking it out of the version control
   system, you will need to run the GNU autotools to regenerate some
   files. (If your directory already contains a file 'config.h.in'
   then this has already been done and you do not need to do it.)
   You can regenerate the files by running the command

   ./autogen.sh

   (If you're doing mmh development, you should look at
   docs/README.developers for related information.)

1) From the top-level source directory, run the command:

       ./configure [options]

   This will check the configuration of your OS, as well as generate
   the various Makefiles.

   The configure script accepts various options. The options of
   most interest are listed in a section below. To see the list
   of all available options, you can run:

       ./configure --help

2) make

   For less terminal output, use: make -s

3) make install

   Note that if you have mmh files in the target directories with
   the same names as the files being installed, the old ones will get
   overwritten without any warning. The only directory this isn't
   true for, is the `etc' directory -- in that directory, the distributed
   files are installed with a `.dist' suffix if they differ from the
   existing file. Watch for information messages while make is processing
   that directory to see if you need to merge changes.

4) You may edit the file `mhn.defaults' in mmh's `etc' directory.

   This file contains the default profile entries for the mmh commands
   mhlist/mhstore/show.  The syntax of this file is described in section
   9.4 of the book "MH & xmh: Email for Users and Programmers", 3rd edition,
   by Jerry Peek, on the Internet at
   <http://rand-mh.sourceforge.net/book/mh/confmhn.html>.
   In most cases you can skip this step.

5) Add the bindir to your PATH variable.

   If you haven't changed any paths, then the bindir is
   `/usr/local/mmh/bin'. Adjust your PATH in ~/.profile, ~/.kshrc,
   ~/.bashrc, or a similar file.

   Have a look at mmhwrap(1), which allows you to access mmh tools
   conveniently without changing the PATH variable.


-----------------------------------------------
Compiler options, or using a different compiler
-----------------------------------------------
By default, configure will use the "gcc" compiler if found. You can use a
different compiler, or add unusual options for compiling or linking that
the "configure" script does not know about, by either editing the user
configuration section of the top level Makefile (after running configure)
or giving "configure" initial values for these in its command line or in
its environment. For example:

    ./configure CC=c89 CFLAGS=-O2 LIBS=-lposix

Or on systems that have the "env" program, you can do it like this:
    env CPPFLAGS=-I/usr/local/include LDFLAGS=-s ./configure

If you want to add to, not replace, compile flags, you can use OURDEFS:
    ./configure OURDEFS='-Wextra -Wno-sign-compare'

---------------------------------
Using a different build directory
---------------------------------
You can compile mmh in a different directory from the one containing
the source code. Doing so allows you to compile it on more than one
architecture at the same time. To do this, you must use a version of
"make" that supports the "VPATH" variable, such as GNU "make". Change
to the directory where you want the object files and executables to go
and run the "configure" script. "configure" automatically checks for
the source code in the directory that "configure" is in. For example,

    cd /usr/local/src/mmh        # source directory
    make clean
    cd /usr/local/solaris/mmh    # build directory
    /usr/local/src/mmh/configure
    make

----------------------------------------
Building mmh on additional architectures
----------------------------------------
To build mmh on additional architectures, you can do a "make distclean".
This should restore the mmh source distribution back to its original
state. You can then configure mmh as above on other architectures in
which you wish to build mmh. Or alternatively, you can use a different
build directory for each architecture.

---------------------
Options for configure
---------------------
--prefix=DIR     (DEFAULT is /usr/local/mmh)
     This will change the base prefix for the installation location
     for the various parts of mmh. Unless overridden, mmh is installed
     in ${prefix}/bin, ${prefix}/etc, ${prefix}/lib, ${prefix}/man.

--bindir=DIR     (DEFAULT is ${prefix}/bin)
     mmh's binaries (show, inc, comp, ...) are installed here.
     You need to have this directory in your PATH variable.

--libdir=DIR     (DEFAULT is ${prefix}/lib)
     mmh's test tools (ap, dp, mhtest, ...) are installed here.
     They are seldom useful to normal users.

--sysconfdir=DIR     (DEFAULT is ${prefix}/etc)
     mmh's global config files (mhn.defaults, ...) are installed here.

--mandir=DIR     (DEFAULT is ${prefix}/man)
     mmh's man pages are installed here.

--enable-debug
     Add debugging symbols to the binaries.

--with-locking=LOCKTYPE    (DEFAULT is dot)
     Specify the locking mechanism when attempting to "inc"
     a local mail spool. Valid options are "dot", "fcntl", "flock",
     and "lockf".

     Of the four, dot-locking requires no special kernel
     or filesystem support, and simply creates a file called
     "FILE.lock" to indicate that "FILE" is locked.
     If LOCKDIR is specified, lock files will be created in that
     directory. Otherwise, lock files will be created in the
     directory where the file being locked resides.

     To configure nmh for kernel locking, use one of the `flock',
     `lockf' or `fcntl' values, each using the equally named
     system call to perform the kernel-level locking.

     In order to be effective, you should contact the site
     administrator to find out what locking mechanisms other
     mail delivery and user programs respect. The most common
     reason not to use dot-locking is if the mail spool directory
     is not world- or user-writeable, and thus a lock file cannot
     be created.

--with-lockdir=DIR
     If you have defined "dot" locking, then the default is to
     place the lock files in the same directory as the file that
     is to be locked. Alternately, if you use this option, you can
     specify that all lock files go in the specified directory.
     Don't define this unless you know you need it.

     This option is only relevant if "dot" locking is used.
     In the other cases, it is ignored.
