mmh (0.3-5) unstable; urgency=medium

  * Add missing symbolic links in bash-complation and alternatives

 -- Dmitry Bogatov <KAction@gnu.org>  Fri, 11 May 2018 18:33:34 +0300

mmh (0.3-4) unstable; urgency=medium

  * Add support for rootless build
  * Avoid conflict with `mh' via alternatives system.
  * Make inc(1) binary setgid mail to allow creating lock files
    in /var/spool/mail
  * User secure protocol when referering to copyright format file
  * Update standards version to 4.1.4 (no changes needed)

 -- Dmitry Bogatov <KAction@gnu.org>  Fri, 04 May 2018 20:42:06 +0000

mmh (0.3-3) unstable; urgency=medium

  * Update Vcs-* fields in debian/control.
  * Compile with large file support
  * Update standards version to 4.1.3 (no changes needed)
  * Bump compat version to 11 (added explicit --no-parallel, since upstream
    build system does not use Automake and is not parallel-safe)
  * Remove useless build-dependency on dh-autoreconf, which is implied by
    debhelper (>= 10)
  * Fix incorrect reference to nmh in description
  * Replace generic BSD-3-clause with specific version from COPYRIGHT file in
    debian/copyright due request of upstream author.
  * Patch config/version.sh to not insert non-reproducible data into binaries

 -- Dmitry Bogatov <KAction@gnu.org>  Sat, 17 Feb 2018 17:32:45 +0300

mmh (0.3-2) unstable; urgency=medium

  * patches/change-manpage-suffix.patch: Install manpages with mh suffix
    to avoid conflicts. (Closes: #838027, #837976)
  * Install bash completion

 -- Dmitry Bogatov <KAction@gnu.org>  Sat, 17 Sep 2016 15:06:05 +0300

mmh (0.3-1) unstable; urgency=low

  * Initial release (Closes: #835276)

 -- Dmitry Bogatov <KAction@gnu.org>  Wed, 24 Aug 2016 09:33:12 +0300
