/*
** whatnowproc.c -- exec the "whatnowproc"
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <unistd.h>


/*
** This routine is used by comp, repl, forw, and dist to exec
** the "whatnowproc".  It first sets up all the environment
** variables that the "whatnowproc" will need to check, and
** then execs the command.
*/
int
what_now(char *ed, int use, char *file, char *altmsg, int dist,
	struct msgs *mp, char *text, char *cwd)
{
	int found, k, msgnum, vecp;
	int len, buflen;
	char *bp;
	char buffer[BUFSIZ], *vec[MAXARGS];

	vecp = 0;
	vec[vecp++] = mhbasename(whatnowproc);
	vec[vecp] = NULL;

	m_putenv("mhdraft", file);
	if (mp)
		m_putenv("mhfolder", mp->foldpath);
	else
		unputenv("mhfolder");
	if (altmsg) {
		if (mp == NULL || *altmsg == '/' || cwd == NULL)
			m_putenv("mhaltmsg", altmsg);
		else {
			snprintf(buffer, sizeof(buffer), "%s/%s",
					mp->foldpath, altmsg);
			m_putenv("mhaltmsg", buffer);
		}
	} else {
		unputenv("mhaltmsg");
	}
	snprintf(buffer, sizeof(buffer), "%d", dist);
	m_putenv("mhdist", buffer);
	if (!ed) {
		m_putenv("mheditor", defaulteditor);
	} else if (*ed) {
		m_putenv("mheditor", ed);
	} else {
		unputenv("mheditor");
	}
	snprintf(buffer, sizeof(buffer), "%d", use);
	m_putenv("mhuse", buffer);

	unputenv("mhmessages");
	unputenv("mhannotate");

	if (text && mp && !is_readonly(mp)) {
		found = 0;
		bp = buffer;
		buflen = sizeof(buffer);
		for (msgnum = mp->lowmsg; msgnum <= mp->hghmsg; msgnum++) {
			if (is_selected(mp, msgnum)) {
				snprintf(bp, buflen, "%s%s", found ? " " : "",
						m_name(msgnum));
				len = strlen(bp);
				bp += len;
				buflen -= len;
				for (k = msgnum + 1; k <= mp->hghmsg && is_selected(mp, k); k++)
					continue;
				if (--k > msgnum) {
					snprintf(bp, buflen, "-%s", m_name(k));
					len = strlen(bp);
					bp += len;
					buflen -= len;
				}
				msgnum = k + 1;
				found++;
			}
		}
		if (found) {
			m_putenv("mhmessages", buffer);
			m_putenv("mhannotate", text);
		}
	}

	context_save();  /* save the context file */
	fflush(stdout);

	if (cwd)
		chdir(cwd);

	execvp(whatnowproc, vec);
	fprintf(stderr, "unable to exec ");
	perror(whatnowproc);

	return 0;
}
