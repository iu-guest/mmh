#!/bin/sh

printhelp()
{
	echo "Usage: $0 command"
	echo "  commands are:"
	echo "  edit [editor]"
	echo "  list"
	echo "  send [sendargs]"
	echo "  delete"
	echo "  display"
	echo "  attach files"
	echo "  alist"
	echo "  detach anum"
	echo "  refile +folder"
	echo "  -help"
	echo "  -Version"
}

version()
{
	if [ $1 -eq 0 ]
	then
		echo "$0 has no own version number, thus this instead:"
		folder -Version
		exit 0
	fi
	echo "$0 has no own version number, thus this instead:" 1>&2
	folder -Version 1>&2
	exit 1
}

usage()
{
	if [ $1 -eq 0 ]
	then
		printhelp
		exit 0
	fi
	printhelp 1>&2
	exit 1
}

get_editor()
{
	if [ -f "$mhmetafile" ]
	then
		lasteditor=`anno -list -component 'last-editor' $mhmetafile`
		if [ -n "$lasteditor" ]
		then
			editor=`echo $lasteditor | cut -d ' ' -f 1`
			mheditor=`mhparam "$editor-next"`
			[ -n "$mheditor" ] && return
			mheditor=$lasteditor
			return
		fi
	fi
	if [ -n "$MMHEDITOR" ]
	then
		mheditor=$MMHEDITOR
		return
	fi
	mheditor=`mhparam 'Editor'`
}

get_showproc()
{
	mhshowproc=`mhparam 'listproc'`
	return
}

get_realpath()
{
	reldir=`dirname "$1"`
	filename=`basename "$1"`
	cd $reldir
	echo "$PWD/$filename"
	cd -
}

get_attachmentheader()
{
	header=`mhparam 'Attachment-Header'`
}

set_lasteditor()
{
	anno -delete -number all -component 'last-editor' $mhmetafile
	anno -nodate -component 'last-editor' -text "$1" $mhmetafile
}

create()
{
	if [ -z "$mhdraft" ]
	then
		usage 1
	fi
	mhmetafile=$mhdraft.meta
	touch $mhmetafile
	if [ -z $mheditor ]
	then
		get_editor
	fi
	if [ -n "$mhaltmsg" ]
	then
		anno -nodate -component 'mhaltmsg' -text "$mhaltmsg" $mhmetafile
	fi
	if [ -n "$mhdist" ]
	then
		anno -nodate -component 'mhdist' -text "$mhdist" $mhmetafile
	fi
	if [ -n "$mhdist" ]
	then
		anno -nodate -component 'mhuse' -text "$mhuse" $mhmetafile
	fi
	if [ -n "$mhfolder" ]
	then
		anno -nodate -component 'mhfolder' -text "$mhfolder" $mhmetafile
	fi
	if [ -n "$mhmessages" ]
	then
		anno -nodate -component 'mhmessages' -text "$mhmessages" $mhmetafile
	fi
	if [ -n "$mhannotate" ]
	then
		anno -nodate -component 'mhannotate' -text "$mhannotate" $mhmetafile
	fi
	set_lasteditor "$mheditor"
	exec $mheditor $mhdraft
}

edit()
{
	if [ $# -eq 0 ]
	then
		get_editor
	else
		mheditor="$@"
	fi

	set_lasteditor "$mheditor"
	exec $mheditor $mhdraft
}

list()
{
	get_showproc
	exec $mhshowproc -file $mhdraft
}

sendfunktion()
{
	export mhaltmsg=`anno -list -component 'mhaltmsg' $mhmetafile`
	export mhdist=`anno -list -component 'mhdist' $mhmetafile`
	export mhuse=`anno -list -component 'mhuse' $mhmetafile`
	export mhfolder=`anno -list -component 'mhfolder' $mhmetafile`
	export mhmessages=`anno -list -component 'mhmessages' $mhmetafile`
	export mhannotate=`anno -list -component 'mhannotate' $mhmetafile`
	send "$@" $mhdraft || exit $?
	rm -f $mhmetafile
	exit 0
}

delete()
{
	folder -push $draftfolder >/dev/null 2>&1
	rmm $draftfolder c
	folder -pop >/dev/null 2>&1
	rm $mhmetafile
}

attach()
{
	get_attachmentheader
	while [ -n "$1" ]
	do
		if [ ! -f "$1" ]
		then
			echo "file not found: $1" 1>&2
			shift
			echo -n "folloing files are not attached: " 1>&2
			echo -n "$1" 1>&2
			echo "$@" 1>&2
			exit 1
		fi
		file=`get_realpath "$1"`
		anno -nodate -append -component $header -text "$file" $mhdraft
		shift
	done
}

alist()
{
	get_attachmentheader
	anno -list -number -component $header $mhdraft
}

detach()
{
	get_attachmentheader
	while [ -n "$1" ]
	do
		anno -delete -component $header -number "$1" $mhdraft
		if [ $? -ne 0 ]
		then
			echo "can't delet attachment $1" 1>&2
			exit 1
		fi
		shift
	done
}

display()
{
	mhaltmsg=`anno -list -component 'mhaltmsg' $mhmetafile`
	get_showproc
	if [ -z "$mhaltmsg" ]
	then
		echo "no altmsg" 1>&2
		exit 1
	fi
	exec $mhshowproc -file $mhaltmsg
}

if [ $# -eq 0 ]
then
	create
	exit
fi

command=$1
shift

draftfolder=`mhparam draftfolder`

mhdraft=`mhpath $draftfolder c 2>/dev/null`
if [ -z "$mhdraft" ]
then
	case $command in
	-h*)
		usage $#
		;;
	-V*)
		version $#
		;;
	*)
		echo "no current message in $draftsfolder" 1>&2
		usage 1
		;;
	esac
fi
mhmetafile=$mhdraft.meta
touch $mhmetafile


case $command in
e|ed|edi|edit)
	edit "$@"
	;;
l|li|lis|list)
	[ $# -eq 0 ] || usage 1
	list
	;;
s|se|sen|send)
	sendfunktion "$@"
	;;
del|dele|delet|delete)
	[ $# -eq 0 ] || usage 1
	delete
	;;
di|dis|disp|displ|displa|display)
	[ $# -eq 0 ] || usage 1
	display
	;;
at|att|atta|attac|attach)
	attach "$@"
	;;
al|ali|alis|alist)
	[ $# -eq 0 ] || usage 1
	alist
	;;
det|deta|detac|detach)
	detach "$@"
	;;
r|re|ref|refi|refil|refile)
	refile -file $mhdraft "$@"
	;;
w|wh|who|whom)
	whom "$@" $mhdraft
	;;
-h|-he|-hel|-help)
	usage $#
	;;
-V|-Ve|-Ver|-Vers|-Versi|-Versio|-Version)
	version $#
	;;
*)
	usage 1
	;;
esac
