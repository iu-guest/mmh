/*
** comp.c -- compose a message
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <h/utils.h>
#include <fcntl.h>
#include <unistd.h>
#include <locale.h>
#include <sysexits.h>

static struct swit switches[] = {
#define EDITRSW  0
	{ "editor editor", 0 },
#define FORMSW  1
	{ "form formfile", 0 },
#define USESW  2
	{ "use", 0 },
#define NUSESW  3
	{ "nouse", 2 },
#define WHATSW  4
	{ "whatnowproc program", 0 },
#define VERSIONSW  5
	{ "Version", 0 },
#define HELPSW  6
	{ "help", 0 },
	{ NULL, 0 }
};


int
main(int argc, char **argv)
{
	int use = NOUSE;
	int in, out;
	char *cp, *cwd, *maildir;
	char *ed = NULL, *form = NULL;
	char *folder = NULL, *msg = NULL, buf[BUFSIZ];
	char drft[BUFSIZ], **argp, **arguments;
	struct msgs *mp = NULL;
	char *fmtstr;

	setlocale(LC_ALL, "");
	invo_name = mhbasename(argv[0]);

	/* read user profile/context */
	context_read();

	arguments = getarguments(invo_name, argc, argv, 1);
	argp = arguments;

	while ((cp = *argp++)) {
		if (*cp == '-') {
			switch (smatch(++cp, switches)) {
			case AMBIGSW:
				ambigsw(cp, switches);
				exit(EX_USAGE);
			case UNKWNSW:
				adios(EX_USAGE, NULL, "-%s unknown", cp);

			case HELPSW:
				snprintf(buf, sizeof(buf), "%s [+folder] [msg] [switches]", invo_name);
				print_help(buf, switches, 1);
				exit(argc == 2 ? EX_OK : EX_USAGE);
			case VERSIONSW:
				print_version(invo_name);
				exit(argc == 2 ? EX_OK : EX_USAGE);

			case EDITRSW:
				if (!(ed = *argp++) || *ed == '-') {
					adios(EX_USAGE, NULL, "missing argument to %s", argp[-2]);
				}
				continue;

			case WHATSW:
				if (!(whatnowproc = *argp++) || *whatnowproc == '-') {
					adios(EX_USAGE, NULL, "missing argument to %s", argp[-2]);
				}
				continue;

			case FORMSW:
				if (!(form = *argp++) || *form == '-') {
					adios(EX_USAGE, NULL, "missing argument to %s", argp[-2]);
				}
				continue;

			case USESW:
				use++;
				continue;
			case NUSESW:
				use = NOUSE;
				continue;
			}
		}
		if (*cp == '+' || *cp == '@') {
			if (folder) {
				adios(EX_USAGE, NULL, "only one folder at a time!");
			} else {
				folder = mh_xstrdup(expandfol(cp));
			}
		} else {
			if (msg) {
				adios(EX_USAGE, NULL, "only one message at a time!");
			} else {
				msg = cp;
			}
		}
	}

	cwd = mh_xstrdup(pwd());

	if (form && (folder || msg)) {
		adios(EX_USAGE, NULL, "can't mix forms and folders/msgs");
	}

	if (use && folder) {
		adios(EX_USAGE, NULL, "can't mix -use and +folder");
	}

	if (use) {
		/* Don't copy; the draft shall get removed in the end. */
		strncpy(drft, m_draft(msg ? msg : seq_cur), sizeof(drft));

	} else if (folder || msg) {
		/* Take a message as the "form" for the new message. */
		if (!msg)
			msg = seq_cur;
		if (!folder)
			folder = getcurfol();
		maildir = toabsdir(folder);
		if (chdir(maildir) == NOTOK) {
			adios(EX_OSERR, maildir, "unable to change directory to");
		}
		/* read folder and create message structure */
		if (!(mp = folder_read(folder))) {
			adios(EX_IOERR, NULL, "unable to read folder %s", folder);
		}
		/* check for empty folder */
		if (mp->nummsg == 0) {
			adios(EX_DATAERR, NULL, "no messages in %s", folder);
		}
		/* parse the message range/sequence/name and set SELECTED */
		if (!m_convert(mp, msg)) {
			exit(EX_SOFTWARE);
		}
		seq_setprev(mp);  /* set the previous-sequence */
		if (mp->numsel > 1) {
			adios(EX_USAGE, NULL, "only one message at a time!");
		}
		if ((in = open(form = mh_xstrdup(m_name(mp->lowsel)),
				O_RDONLY)) == NOTOK) {
			adios(EX_IOERR, form, "unable to open message");
		}

		strncpy(drft, m_draft(seq_beyond), sizeof(drft));
		if ((out = creat(drft, m_gmprot())) == NOTOK) {
			adios(EX_CANTCREAT, drft, "unable to create");
		}
		cpydata(in, out, form, drft);
		close(in);
		close(out);

	} else {
		fmtstr = new_fs(form, components);
		strncpy(drft, m_draft(seq_beyond), sizeof(drft));
		if ((out = creat(drft, m_gmprot())) == NOTOK) {
		adios(EX_CANTCREAT, drft, "unable to create");
		}
		if (write(out, fmtstr, strlen(fmtstr)) != (int)strlen(fmtstr)) {
			adios(EX_IOERR, drft, "error writing");
		}
		close(out);
	}

	context_save();
	what_now(ed, use, drft, NULL, 0, NULLMP, NULL, cwd);
	return EX_OSERR;
}
