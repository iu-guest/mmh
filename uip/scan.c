/*
** scan.c -- display a one-line "scan" listing of folder or messages
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <h/fmt_scan.h>
#include <h/scansbr.h>
#include <h/tws.h>
#include <h/utils.h>
#include <errno.h>
#include <unistd.h>
#include <locale.h>
#include <sysexits.h>

static struct swit switches[] = {
#define FORMSW  0
	{ "form formatfile", 0 },
#define WIDTHSW  1
	{ "width columns", 0 },
#define FILESW  2
	{ "file file", 0 },
#define VERSIONSW 3
	{ "Version", 0 },
#define HELPSW  4
	{ "help", 0 },
	{ NULL, 0 }
};


int
main(int argc, char **argv)
{
	int width = 0;
	int i, state, msgnum;
	int seqnum[NUMATTRS], unseen, num_unseen_seq = 0;
	char *cp, *maildir, *file = NULL, *folder = NULL;
	char *form = NULL, buf[BUFSIZ];
	char **argp, *fmtstr, **arguments;
	struct msgs_array msgs = { 0, 0, NULL };
	struct msgs *mp;
	FILE *in;

	setlocale(LC_ALL, "");
	invo_name = mhbasename(argv[0]);

	/* read user profile/context */
	context_read();

	arguments = getarguments(invo_name, argc, argv, 1);
	argp = arguments;

	/*
	** Parse arguments
	*/
	while ((cp = *argp++)) {
		if (*cp == '-') {
			switch (smatch(++cp, switches)) {
			case AMBIGSW:
				ambigsw(cp, switches);
				exit(EX_USAGE);
			case UNKWNSW:
				adios(EX_USAGE, NULL, "-%s unknown", cp);

			case HELPSW:
				snprintf(buf, sizeof(buf), "%s [+folder] [msgs] [switches]", invo_name);
				print_help(buf, switches, 1);
				exit(argc == 2 ? EX_OK : EX_USAGE);
			case VERSIONSW:
				print_version(invo_name);
				exit(argc == 2 ? EX_OK : EX_USAGE);

			case FORMSW:
				if (!(form = *argp++) || *form == '-')
					adios(EX_USAGE, NULL, "missing argument to %s",
							argp[-2]);
				continue;

			case WIDTHSW:
				if (!(cp = *argp++) || *cp == '-')
					adios(EX_USAGE, NULL, "missing argument to %s",
							argp[-2]);
				width = atoi(cp);
				continue;

			case FILESW:
				if (!(cp = *argp++) || (cp[0] == '-' && cp[1]))
					adios(EX_USAGE, NULL, "missing argument to %s",
							argp[-2]);
				if (strcmp(file = cp, "-")!=0)
					file = mh_xstrdup(expanddir(cp));
				continue;
			}
		}
		if (*cp == '+' || *cp == '@') {
			if (folder)
				adios(EX_USAGE, NULL, "only one folder at a time!");
			else
				folder = mh_xstrdup(expandfol(cp));
		} else
			app_msgarg(&msgs, cp);
	}

	/* Set format string.  Must be before chdir(). */
	fmtstr = new_fs(form, scanformat);

	/*
	** We are scanning a maildrop file
	*/
	if (file) {
		if (msgs.size)
			adios(EX_USAGE, NULL, "\"msgs\" not allowed with -file");
		if (folder)
			adios(EX_USAGE, NULL, "\"+folder\" not allowed with -file");

		/* check if "file" is really stdin */
		if (strcmp(file, "-") == 0) {
			in = stdin;
			file = "stdin";
		} else if (!(in = fopen(file, "r"))) {
			adios(EX_IOERR, file, "unable to open");
		}

		for (msgnum = 1; ; ++msgnum) {
			state = scan(in, msgnum, SCN_MBOX, fmtstr, width, 0, 0);
			if (state != SCNMSG)
				break;
		}
		fclose(in);
		exit(EX_OK);
	}

	/*
	** We are scanning a folder
	*/

	if (!msgs.size)
		app_msgarg(&msgs, seq_all);
	if (!folder)
		folder = getcurfol();
	maildir = toabsdir(folder);

	if (chdir(maildir) == NOTOK)
		adios(EX_OSERR, maildir, "unable to change directory to");

	if (!(mp = folder_read(folder)))
		adios(EX_IOERR, NULL, "unable to read folder %s", folder);

	/* check for empty folder */
	if (mp->nummsg == 0)
		adios(EX_DATAERR, NULL, "no messages in %s", folder);

	/* parse all the message ranges/sequences and set SELECTED */
	for (msgnum = 0; msgnum < msgs.size; msgnum++)
		if (!m_convert(mp, msgs.msgs[msgnum]))
			exit(EX_USAGE);
	seq_setprev(mp);

	context_replace(curfolder, folder);
	seq_save(mp);
	context_save();

	/*
	** Get the sequence number for each `unseen' sequence
	*/
	if (!(cp = context_find(usequence))) {
		cp = seq_unseen;  /* use default, if not set */
	}
	if (*cp) {
		char **ap, *dp;

		dp = mh_xstrdup(cp);
		ap = brkstring(dp, " ", "\n");
		for (i = 0; ap && *ap; i++, ap++) {
			seqnum[i] = seq_getnum(mp, *ap);
		}
		num_unseen_seq = i;
		if (dp) {
			mh_free0(&dp);
		}
	}

	for (msgnum = mp->lowsel; msgnum <= mp->hghsel; msgnum++) {
		if (!is_selected(mp, msgnum)) {
			continue;
		}

		if ((in = fopen(cp = m_name(msgnum), "r")) == NULL) {
			admonish(cp, "unable to open message");
			continue;
		}

		/*
		** Check if message is in any sequence given
		** by Unseen-Sequence profile entry.
		*/
		unseen = 0;
		for (i = 0; i < num_unseen_seq; i++) {
			if (in_sequence(mp, seqnum[i], msgnum)) {
				unseen = 1;
				break;
			}
		}

		switch (state = scan(in, msgnum, SCN_FOLD, fmtstr,
				width, msgnum==mp->curmsg, unseen)) {
		case SCNMSG:
		case SCNERR:
			break;

		default:
			adios(EX_SOFTWARE, NULL, "scan() botch(%d)", state);

		case SCNEOF:
			advise(NULL, "message %d: empty", msgnum);
			break;
		}
		fclose(in);
	}
	folder_free(mp);

	return 0;
}
