.\"
.\" %nmhwarning%
.\"
.TH MHPGP %manext1% "%nmhdate%" MH.6.8 [%nmhversion%]
.SH NAME
mhpgp \- check PGP signatures and decrypt PGP messages using gnupg
.SH SYNOPSIS
.HP 5
.na
.B mhpgp
.RB [ \-write ]
.RB [ \-Version ]
.RB [ \-help ]
.RI [ +folder ]
.RI [ msg ]
.ad
.SH DESCRIPTION
.B mhpgp
is a script to simplify verifying and decrypting PGP messages,
using gnupg.
.PP
.B mhpgp
can handle signatures and encryption in MIME and plain (non-MIME) format.
Signed messages are verified.
Encrypted messages are displayed with
.B show
in decrypted form, the signature is verified as well.
.PP
With the
.B \-write
switch, the decrypted message is stored into the current folder.
.PP
If no
.I msg
is given, the current message is used.
.PP
Trailing blanks are stripped from the lines before signature verification,
because non are expected to be present as RFC 3156 requests:
.PP
.RS 5
[...] implementations MUST make sure that no trailing
whitespace is present after the MIME encoding has been applied.
.RE
.PP
If there is trailing whitespace, it was likely added mistakenly
during mail transfer.

.SH FILES
None

.SH "PROFILE COMPONENTS"
None

.SH "SEE ALSO"
mhsign(1), gpg(1)

.SH DEFAULTS
.nf
.RB ` +folder "' defaults to the current folder"
.RB ` msg "' defaults to the current message"
.fi

.SH CONTEXT
None

.SH BUGS
The order of the command line arguments is relevant: The options
must come first, the message specification last.
