.\"
.\" %nmhwarning%
.\"
.TH MH-MAIL %manext5% "%nmhdate%" MH.6.8 [%nmhversion%]
.SH NAME
mh-mail \- message format for mh message system
.SH DESCRIPTION
.B mmh
processes messages in a particular format.  It should be noted
that although neither Bell nor Berkeley mailers produce message files
in the format that
.B mmh
prefers,
.B mmh
can read message files in that antiquated format.
.PP
Each user possesses a system maildrop box which initially receives all
messages delivered by the MTA.
.B Inc
will read from that maildrop
and incorporate the new messages found there into the user's own
mail folders (typically
.RI ` +inbox ').
.PP
Messages are expected to consist of lines of text.  Graphics and binary
data are not directly handled.  No data compression is accepted.  All text is
clear ASCII 7-bit data.
.PP
The general `memo' framework of RFC\-822 is used.  A message
consists of a block of information in a rigid format, followed by
general text with no specified format.  The rigidly formatted first
part of a message is called the header, and the free-format portion is
called the body.  The header must always exist, but the body is optional.
These parts are separated by an empty line, i.e., two consecutive newline
characters.  Within
.B mmh ,
the header and body may be separated by a line consisting of dashes:
.PP
.RS 5
.nf
%components%
.fi
.RE
.PP
The header is composed of one or more header items.  Each header item can
be viewed as a single logical line of ASCII characters.  If the text of
a header item extends across several real lines, the continuation lines
are indicated by leading spaces or tabs.
.PP
Each header item is called a component and is composed of a keyword or
name, along with associated text.  The keyword begins at the left margin,
may NOT contain spaces or tabs, may not exceed 63 characters (as specified
by RFC\-822), and is terminated by a colon (`:').  Certain components
(as identified by their keywords) must follow rigidly defined formats
in their text portions.
.PP
The text for most formatted components (e.g., `Date:' and
`Message\-Id:') is produced automatically.  The only ones entered
by the user are address fields such as `To:', `Cc:',
etc.  Internet addresses are assigned mailbox names and host computer
specifications.  The rough format is `local@domain', such as
`bob@example.org'.  Multiple addresses
are separated by commas.  A missing host/domain is assumed to be the
local host/domain.
.PP
As mentioned above, a blank line (or a line of dashes) signals that all
following text up to the end of the file is the body.  No formatting is
expected or enforced within the body.
.PP
Following is a list of header components that are considered
meaningful to various
.B mmh
programs.
.PP
.BR Date :
.RS 5
Added by
.BR spost .
Contains date and time of the message's entry
into the mail transport system.
.RE
.PP
.BR From :
.RS 5
Added by
.BR spost .
Contains the address of the author or authors
(may be more than one if a `Sender:' field is present).  For a
standard reply (using
.BR repl ,
the reply address is constructed by
checking the following headers (in this order): `Mail-Reply\-To:',
`Reply\-To:', `From:', `Sender:'.
.RE
.PP
.BR Mail\-Reply\-To :
.RS 5
For a standard reply (using
.BR repl ),
the reply address is
constructed by checking the following headers (in this order):
`Mail-Reply\-To:', `Reply\-To:', `From:',
`Sender:'.
.RE
.PP
.BR Mail\-Followup\-To :
.RS 5
When making a `group' reply (using
.B repl
.BR \-group ),
any addresses in this field will take precedence, and no other reply address
will be added to the draft.  If this header is not available, then the
return addresses will be constructed from the `Mail-Reply\-To:',
or `Reply\-To:', or `From:', along with adding the
addresses from the headers `To:', `Cc:', as well as
adding your personal address.
.RE
.PP
.BR Reply\-To :
.RS 5
For a standard reply (using
.BR repl ),
the reply address is
constructed by checking the following headers (in this order):
`Mail-Reply\-To:', `Reply\-To:', `From:',
`Sender:'.
.RE
.PP
.BR Sender :
.RS 5
Added by
.B spost
in the event that the message already has a
`From:' line.  This line contains the address of the actual
sender.
.RE
.PP
.BR To :
.RS 5
Contains addresses of primary recipients.
.RE
.PP
.BR Cc :
.RS 5
Contains addresses of secondary recipients.
.RE
.PP
.BR Bcc :
.RS 5
Still more recipients.  However, the `Bcc:' line is not
copied onto the message as delivered, so these recipients are not
listed.
.B mmh
uses an encapsulation method for blind copies, see
.BR send (1)
or
.BR spost (8).
.RE
.PP
.BR Fcc :
.RS 5
Causes
.B spost
to copy the message into the specified folder for the sender,
if the message was successfully given to the transport system.
.RE
.PP
.BR Message\-ID :
.RS 5
A unique message identifier added by the MTA.
.RE
.PP
.BR Subject :
.RS 5
Sender's commentary.  It is displayed by
.BR scan .
.RE
.PP
.BR In\-Reply\-To :
.RS 5
A commentary line added by
.B repl
when replying to a message.
.RE
.PP
.BR Resent\-Date :
.RS 5
Added when redistributing a message by
.BR spost .
.RE
.PP
.BR Resent\-From :
.RS 5
Added when redistributing a message by
.BR spost .
.RE
.PP
.BR Resent\-To:
.RS 5
New recipients for a message resent by
.BR dist .
.RE
.PP
.BR Resent\-Cc :
.RS 5
Still more recipients. See `Cc:' and `Resent\-To:'.
.RE
.PP
.BR Resent\-Bcc :
.RS 5
Even more recipients. See `Bcc:' and `Resent\-To:'.
.RE
.PP
.BR Resent\-Fcc :
.RS 5
Copy resent message into a folder.
See `Fcc:' and `Resent\-To:'.
.RE
.PP
.BR Resent\-Message\-Id :
.RS 5
A unique identifier glued on by the MTA.
See `Message\-Id:' and `Resent\-To:'.
.RE
.PP
The following non-standard header components are also meaningful to
.B mmh
tools:
.PP
.BR Attach :
.RS 5
Annotation for
.B send
to attach the given file to the message being sent.
.RE
.PP
.BR Replied :
.RS 5
Annotation for
.B repl
under the
.B \-annotate
option.
.RE
.PP
.BR Forwarded :
.RS 5
Annotation for
.B forw
under the
.B \-annotate
option.
.RE
.PP
.BR Resent :
.RS 5
Annotation for
.B dist
under the
.B \-annotate
option.
.RE

.SH FILES
.fc ^ ~
.nf
.ta \w'%etcdir%/ExtraBigFileName  'u
^%mailspool%/$USER~^Location of mail drop
.fi

.SH "SEE ALSO"
.I "Standard for the Format of ARPA Internet Text Messages
(RFC\-822)

.SH CONTEXT
None
