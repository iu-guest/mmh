.\"
.\" %nmhwarning%
.\"
.TH WHOM %manext1% "%nmhdate%" MH.6.8 [%nmhversion%]
.SH NAME
whom \- report to whom a message would go
.SH SYNOPSIS
.HP 5
.na
.B whom
.RB [ \-tocc " | " \-notocc ]
.RB [ \-dcc " | " \-nodcc ]
.RB [ \-bcc " | " \-nobcc ]
.RB [ \-alias " | " \-noalias ]
.RB [ \-Version ]
.RB [ \-help ]
.IR file ...
.ad
.SH DESCRIPTION
.B Whom
is used to list the recipient addresses in the headers of a message.
.PP
Per default,
.B whom
lists sighted and hidden recipients.
The
.BR \-notocc
option suppresses the listing of sighted recipients (To and Cc).
The
.BR \-nodcc
option suppresses the listing of hidden DCC recipients.
The
.BR \-nobcc
option suppresses the listing of hidden BCC recipients.
If the printing of multiple kinds of recipients
is requested and hidden ones are present,
they are separated by a line similar to ``\0==DCC=='' or ``\0==BCC==''.
The actual separator may change, but the regular expression
/^[\0\\t].*[DB]CC/ should always match.
No separator is printed if only one kind of recipients is requested
for printing, or if multiple are requested but no hidden recipients are
present.
.PP
With
.BR \-alias ,
aliases for all recipients are expanded, according to the
alias files given by the `Aliasfile:' profile entry.
This is somehow similar to running:

.RS 5
.nf
ali `whom /path/to/msg`
.fi
.RE

.SH FILES
.fc ^ ~
.nf
.ta \w'%etcdir%/ExtraBigFileName  'u
^$HOME/.mmh/profile~^The user profile
.fi

.SH "PROFILE COMPONENTS"
.fc ^ ~
.nf
.ta 2.4i
.ta \w'ExtraBigProfileName  'u
^Path:~^To determine the user's mail storage
^Aliasfile:~^For default alias files
.fi

.SH "SEE ALSO"
mh\-alias(5), spost(8)

.SH DEFAULTS
.nf
.RB ` \-tocc '
.RB ` \-dcc '
.RB ` \-bcc '
.RB ` \-alias '
.fi

.SH CONTEXT
None

.SH BUGS
.B whom
does not (yet) accept
.I msg
and
.I +folder
arguments.
