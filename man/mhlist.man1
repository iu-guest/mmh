.\"
.\" %nmhwarning%
.\"
.TH MHLIST %manext1% "%nmhdate%" MH.6.8 [%nmhversion%]
.SH NAME
mhlist \- list information about MIME messages
.SH SYNOPSIS
.HP 5
.na
.B mhlist
.RI [ +folder ]
.RI [ msgs ]
.RB [ \-file
.IR file ]
.RB [ \-part
.IR number ]
\&...
.RB [ \-type
.IR content ]
\&...
.RB [ \-verbose " | " \-noverbose ]
.RB [ \-Version ]
.RB [ \-help ]
.ad
.SH DESCRIPTION
The
.B mhlist
command allows you to list information (essentially
a table of contents) about the various parts of a collection of
MIME (multi-media) messages.
.PP
.B mhlist
manipulates MIME (multi-media messages) as specified
in RFC\-2045 thru RFC\-2049 (See
.BR mhbuild (1)).
.PP
A one-line banner is displayed above the listing.
The size of the
`native' (decoded) format of each content is evaluated.
This provides an accurate count at the expense of a small delay.
If the
.B \-verbose
switch is present, then the listing will show
any `extra' information that is present in the message,
such as comments in the `Content-Type' header.
.PP
The option
.B \-file
.I file
directs
.B mhlist
to use the specified
file as the source message, rather than a message from a folder.
If you specify this file as `-', then
.B mhlist
will
accept the source message on the standard input.  Note that the
file, or input from standard input should be a validly formatted
message, just like any other
.B mh
message.  It should
.B NOT
be in mail drop format (to convert a file in mail drop format to
a folder of
.B mh
messages, see
.BR inc (1)).
.PP
By default,
.B mhlist
will list information about the entire
message (all of its parts).  By using the
.B \-part
and
.B \-type
switches, you may limit the scope of this command to particular
subparts (of a multipart content) and/or particular content types.
.PP
A part specification consists of a series of numbers separated by dots.
For example, in a multipart content containing three parts, these
would be named as 1, 2, and 3, respectively.  If part 2 was also a
multipart content containing two parts, these would be named as 2.1 and
2.2, respectively.  Note that the
.B \-part
switch is effective for only
messages containing a multipart content.  If a message has some other
kind of content, or if the part is itself another multipart content, the
.B \-part
switch will not prevent the content from being acted upon.
.PP
A content specification consists of a content type and a subtype.
The initial list of `standard' content types and subtypes can
be found in RFC\-2046.
.PP
A list of commonly used contents is briefly reproduced here:
.PP
.RS 5
.nf
.ta \w'application  'u
Type	Subtypes
----	--------
text	plain, enriched
multipart	mixed, alternative, digest, parallel
message	rfc822, partial, external-body
application	octet-stream, postscript
image	jpeg, gif, png
audio	basic
video	mpeg
.fi
.RE
.PP
A legal MIME message must contain a subtype specification.
.PP
To specify a content, regardless of its subtype, just use the
name of the content, e.g., `audio'.  To specify a specific
subtype, separate the two with a slash, e.g., `audio/basic'.
Note that regardless of the values given to the
.B \-type
switch, a
multipart content (of any subtype listed above) is always acted upon.

.SH FILES
.fc ^ ~
.nf
.ta \w'%etcdir%/ExtraBigFileName  'u
^$HOME/.mmh/profile~^The user profile
.fi

.SH "PROFILE COMPONENTS"
.fc ^ ~
.nf
.ta 2.4i
.ta \w'ExtraBigProfileName  'u
^Path:~^To determine the user's mail storage
^Current\-Folder:~^To find the default current folder
.fi

.SH "SEE ALSO"
mhbuild(1), show(1), mhstore(1), sendfiles(1)

.SH DEFAULTS
.nf
.RB ` +folder "' defaults to the current folder"
.RB ` msgs "' defaults to the current message"
.RB ` \-noverbose '
.fi

.SH CONTEXT
If a folder is given, it will become the current folder.  The last
message selected will become the current message.
