.\"
.\" %nmhwarning%
.\"
.TH PACKF %manext1% "%nmhdate%" MH.6.8 [%nmhversion%]
.SH NAME
packf \- pack messages in mh folder into a single mbox file
.SH SYNOPSIS
.HP 5
.na
.B packf
.RI [ +folder ]
.RI [ msgs ]
.RB [ \-Version ]
.RB [ \-help ]
.ad
.SH DESCRIPTION
.B Packf
will pack copies of messages from a folder, into mbox format and
print it to the standard output.
.PP
.B packf
makes an mbox-style delimiter by examining the first line
of the message.  If the first line is a `Return-Path'
field, its address and the current date and time are used.  Otherwise,
if the first line has an `X-Envelope-From' field, its
contents (which should already be in the correct format) are used.
Otherwise, a dummy address and the current date and time are used.
.PP
Messages that are packed by
.B packf
can be unpacked using
.BR inc .

.SH FILES
.fc ^ ~
.nf
.ta \w'%etcdir%/ExtraBigFileName  'u
^$HOME/.mmh/profile~^The user profile
.fi

.SH "PROFILE COMPONENTS"
.fc ^ ~
.nf
.ta 2.4i
.ta \w'ExtraBigProfileName  'u
^Path:~^To determine the user's mail storage
^Current\-Folder:~^To find the default current folder
.fi

.SH "SEE ALSO"
inc(1)

.SH DEFAULTS
.nf
.RB ` +folder "' defaults to the current folder"
.RB ` msgs "' defaults to all"
.fi

.SH CONTEXT
If a folder is given, it will become the current folder.  The first
message packed will become the current message.
