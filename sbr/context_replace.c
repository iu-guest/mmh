/*
** context_replace.c -- add/replace an entry in the context/profile list
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <h/utils.h>


void
context_replace(char *key, char *value)
{
	struct node *np;

	/*
	** If list is emtpy, allocate head of profile/context list.
	*/
	if (!m_defs) {
		m_defs = mh_xcalloc(1, sizeof(*np));

		np = m_defs;
		np->n_name = mh_xstrdup(key);
		np->n_field = mh_xstrdup(value);
		np->n_context = 1;
		np->n_next = NULL;
		ctxflags |= CTXMOD;
		return;
	}

	/*
	** Search list of context/profile entries for
	** this key, and replace its value if found.
	*/
	for (np = m_defs;; np = np->n_next) {
		if (!mh_strcasecmp(np->n_name, key)) {
			if (strcmp(value, np->n_field)!=0) {
				if (!np->n_context)
					admonish(NULL, "bug: context_replace(key=\"%s\",value=\"%s\")", key, value);
				if (np->n_field)
					mh_free0(&(np->n_field));
				np->n_field = mh_xstrdup(value);
				ctxflags |= CTXMOD;
			}
			return;
		}
		if (!np->n_next)
			break;
	}

	/*
	** Else add this new entry at the end
	*/
	np->n_next = mh_xcalloc(1, sizeof(*np));

	np = np->n_next;
	np->n_name = mh_xstrdup(key);
	np->n_field = mh_xstrdup(value);
	np->n_context = 1;
	np->n_next = NULL;
	ctxflags |= CTXMOD;
}
