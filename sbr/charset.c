/*
** charset.c -- routines for character sets
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#ifdef HAVE_LANGINFO_H
# include <langinfo.h>
#endif


/*
** Get the current character set
*/
char *
get_charset()
{
	char *charset = getenv("MM_CHARSET");
#if defined(HAVE_NL_LANGINFO) && defined(CODESET)
	if (!charset) {
		charset = norm_charmap(nl_langinfo(CODESET));
	}
#endif
	return charset;
}


/*
** Check if we can display a given character set natively.
*/
int
is_native_charset(char *str)
{
	char *mm_charset = NULL;

	if (!(mm_charset = get_charset())) {
		mm_charset = "US-ASCII";
	}
	if (mh_strcasecmp(str, mm_charset)==0) {
		return 1;
	}

	/* US-ASCII is a subset of the ISO-8859-X and UTF-8 character sets */
	if (strncasecmp("ISO-8859-", mm_charset, 9)==0 ||
			mh_strcasecmp("UTF-8", mm_charset)==0) {
		if (mh_strcasecmp(str, "US-ASCII")==0) {
			return 1;
		}
	}
	return 0;
}


/*
** Return the name of the character set we are
** using for 8bit text.
*/
char *
write_charset_8bit(void)
{
	char *mm_charset = NULL;

	if (!(mm_charset = get_charset())) {
		mm_charset = "x-unknown";
	}
	return mm_charset;
}
