/*
** mhbasename.c -- Given a path name, return a pointer to the character
**              -- after the last slash. If none present, return the the
**              -- beginning of the path.
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>


/*
**  Note: In contrast to POSIX basename(), we don't handle trailing
**  slashes special. If path has a trailing slash, we return a pointer
**  to a null byte (i.e. to an empty string). Also different: We don't
**  modify the original string neither do we return a pointer to static
**  memory.
*/
char *
mhbasename(char *path)
{
	char *cp;

	if (!path) {
		return NULL;
	}
	cp = strrchr(path, '/');
	return (!cp) ? path : ++cp;
	
}
