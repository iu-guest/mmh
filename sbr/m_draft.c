/*
** m_draft.c -- construct the name of a draft message
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <h/utils.h>
#include <errno.h>
#include <sysexits.h>


/*
**  `which' should either be the cur sequence to use the current draft
**  or the beyond sequence to start with a new draft.
*/
char *
m_draft(char *which)
{
	struct msgs *mp;
	static char buffer[BUFSIZ];
	char *folder;

	folder = mh_xstrdup(toabsdir(draftfolder));
	create_folder(folder, 0, exit);
	if (!(mp = folder_read(folder))) {
		adios(EX_IOERR, NULL, "unable to read folder %s", folder);
	}
	mh_free0(&folder);

	/*
	** Make sure we have enough message status space for all
	** the message numbers from 1 to one beyond last, since we might
	** select an empty slot.  If we add more space at the
	** end, go ahead and add 10 additional slots.
	*/
	if (mp->hghmsg >= mp->hghoff) {
		if (!(mp = folder_realloc(mp, 1, mp->hghmsg + 10)))
			adios(EX_OSERR, NULL, "unable to allocate folder storage");
	} else if (mp->lowoff > 1) {
		if (!(mp = folder_realloc(mp, 1, mp->hghoff)))
			adios(EX_OSERR, NULL, "unable to allocate folder storage");
	}

	mp->msgflags |= ALLOW_BEYOND;  /* allow the beyond sequence */

	/*
	** The draft message name to return is defined by `which'.
	** Usually it is seq_cur (for the current draft) or seq_beyond
	** (to start a new draft).
	*/
	if (!m_convert(mp, which))
		exit(EX_SOFTWARE);
	seq_setprev(mp);

	snprintf(buffer, sizeof(buffer), "%s/%s", mp->foldpath,
			m_name(mp->lowsel));
	seq_setcur(mp, mp->lowsel);
	seq_save(mp);
	folder_free(mp);

	return buffer;
}
