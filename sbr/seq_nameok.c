/*
** seq_nameok.c -- check if a name is ok for a user-defined sequence
**
** This code is Copyright (c) 2002, by the authors of nmh.  See the
** COPYRIGHT file in the root directory of the nmh distribution for
** complete copyright information.
*/

#include <h/mh.h>
#include <ctype.h>


/*
** returns true if it is a valid name for a user-defined sequence
*/
int
seq_nameok(unsigned char *s)
{
	unsigned char *pp;

	if (s == NULL || *s == '\0') {
		advise(NULL, "empty sequence name");
		return 0;
	}

	/*
	** Make sure sequence name doesn't clash with one
	** of the `reserved' sequence names.
	** Note: Accept `cur' here! But why is it treated special? --meillo
	*/
	if (strcmp(s, seq_first)==0 || strcmp(s, seq_last)==0 ||
			strcmp(s, seq_prev)==0 || strcmp(s, seq_next)==0 ||
			strcmp(s, seq_all)==0 || strcmp(s, seq_beyond)==0) {
		advise(NULL, "collision with reserved sequence name: `%s'", s);
		return 0;
	}

	/*
	** First character in a sequence name must be
	** an alphabetic character ...
	*/
	if (!isalpha(*s)) {
		advise(NULL, "sequence name must start with a letter: %s", s);
		return 0;
	}

	/*
	** and can be followed by zero or more alphanumeric characters
	*/
	for (pp = s+1; *pp; pp++) {
		if (!isalnum(*pp)) {
			advise(NULL, "sequence name must only contain "
					"letters and digits: %s", s);
			return 0;
		}
	}

	return 1;
}
