#
# Makefile for top level of mmh distribution
#

# mmh version
VERSION = @VERSION@

SHELL = /bin/sh
@SET_MAKE@

srcdir = @srcdir@
VPATH  = @srcdir@

# ========== USER CONFIGURATION SECTION ==========
#
# If `make' is executed in the directory containing this Makefile,
# any changes made in this section will override the values of
# these parameters in makefiles in any of the subdirectories.

prefix      = @prefix@
exec_prefix = @exec_prefix@

# location of standard commands
bindir      = @bindir@

# location of support binaries and scripts
libdir      = @libdir@

# location of mmh configuration and formats files
etcdir      = @sysconfdir@

# location of ...
datarootdir = @datarootdir@

# location of man pages
mandir      = @mandir@

# location of incoming mail
mailspool   = @mailspool@

# location of mail transport agent
sendmailpath = @sendmailpath@

CC       = @CC@
CPPFLAGS = @CPPFLAGS@
DEFS     = @DEFS@ @OURDEFS@
CFLAGS   = @CFLAGS@
LDFLAGS  = @LDFLAGS@
LIBS     = @LIBS@

# ========== YOU SHOULDN'T HAVE TO CHANGE ANYTHING BELOW HERE ==========

# flags passed to recursive makes in subdirectories
MAKEDEFS = CC='$(CC)' CPPFLAGS='$(CPPFLAGS)' DEFS='$(DEFS)' \
CFLAGS='$(CFLAGS)' LDFLAGS='$(LDFLAGS)' LIBS='$(LIBS)' \
prefix='$(prefix)' exec_prefix='$(exec_prefix)' bindir='$(bindir)' \
etcdir='$(etcdir)' libdir='$(libdir)' mandir='$(mandir)' \
mailspool='$(mailspool)' sendmailpath='$(sendmailpath)'

INSTALL         = @INSTALL@
INSTALL_PROGRAM = @INSTALL_PROGRAM@
INSTALL_DATA    = @INSTALL_DATA@

.SUFFIXES:

# distribute all files that are under version control ...
# ... plus (for convenience):
DISTINCLUDE = config.h.in configure stamp-h.in sbr/dtimep.c ChangeLog
# ... minus (git stuff):
DISTEXCLUDE = .git .gitignore

# Subdirectories with own Makefiles
SUBDIRS = h config sbr uip etc man

# ========== DEPENDENCIES FOR BUILDING AND INSTALLING ==========

# default target
all: config.h Makefile all-recursive

all-recursive:
	for subdir in $(SUBDIRS); do \
	  (cd $$subdir && $(MAKE) $(MAKEDEFS) all) || exit 1; \
	done

install: all
	for subdir in $(SUBDIRS); do \
	  (cd $$subdir && $(MAKE) $(MAKEDEFS) $@) || exit 1; \
	done

uninstall:
	for subdir in $(SUBDIRS); do \
	  (cd $$subdir && $(MAKE) $(MAKEDEFS) $@) || exit 1; \
	done

# ========== DEPENDENCIES FOR CLEANUP ==========

mostlyclean: mostlyclean-recursive mostlyclean-local
clean:       clean-recursive       clean-local
distclean:   distclean-recursive   distclean-local
realclean:   realclean-recursive   realclean-local
superclean:  superclean-recursive  superclean-local

mostlyclean-local:
	rm -f *~
	rm -rf autom4te.cache

clean-local: mostlyclean-local

distclean-local: clean-local
	rm -f Makefile config.h config.status config.log config.cache stamp-h distname ChangeLog

realclean-local: distclean-local

superclean-local: realclean-local
	cd $(srcdir) && rm -f config.h.in stamp-h.in configure

mostlyclean-recursive clean-recursive distclean-recursive realclean-recursive superclean-recursive:
	for subdir in $(SUBDIRS); do \
	  target=`echo $@ | sed 's/-recursive//'`; \
	  (cd $$subdir && $(MAKE) $(MAKEDEFS) $$target) || exit 1; \
	done

# ========== DEPENDENCIES FOR MAINTENANCE ==========

lint:
	for subdir in $(SUBDIRS); do \
	  ( cd $$subdir && $(MAKE) $(MAKEDEFS) lint ) || exit 1; \
	done

# ========== DEPENDENCIES FOR MAINTENANCE ==========

Makefile: Makefile.in config.status
	./config.status $@

config.status: configure VERSION
	./config.status --recheck

configure: configure.ac
	cd $(srcdir) && autoconf

config.h: stamp-h
stamp-h: config.h.in config.status
	./config.status config.h stamp

config.h.in: stamp-h.in
stamp-h.in: configure.ac
	cd $(srcdir) && autoheader
	date > $@

# rebuild all autoconf files
reset:
	cd $(srcdir) && autoheader
	cd $(srcdir) && autoconf
	cd $(srcdir) && date > stamp-h.in

# name of new mmh distribution tar file
tarfile = mmh-$(VERSION).tar.gz

# file containing name of new mmh distribution
distname:
	@echo mmh-$(VERSION) > distname

# pre-generate this file so users don't need lex(1)
sbr/dtimep.c:
	@cd $(srcdir)/sbr && $(MAKE) dtimep.c

ChangeLog:
	@git whatchanged --format=fuller > $@

# build mmh distribution
distdir = `cat distname`
mmhdist: $(DISTINCLUDE) distname
	@echo "Begin building mmh-$(VERSION) distribution"
	@rm -rf $(distdir)
	@echo "Copying files to distribution directory"
	@git clone $(srcdir) $(distdir)
	@chmod 755 $(distdir)
	@for file in $(DISTINCLUDE); do \
	  cp -p $(srcdir)/$$file $(distdir)/$$file; \
	  touch $(distdir)/$$file; \
	done
	@for file in $(DISTEXCLUDE); do \
	  rm -rf $(distdir)/$$file; \
	done
	@chmod -R a+r $(distdir)
	tar chf - $(distdir) | gzip -c > $(tarfile)
	@rm -rf $(distdir) distname
	@echo "Done building mmh-$(VERSION) distribution"
