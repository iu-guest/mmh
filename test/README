Mmh test suite.

The purpose of these tests is to verify the functionality of the mmh
commands.  The goal of the suite is to create an environment where testing
mmh commands is easy and useful.  Each test is a shell script, and is
launched via the 'sh' command. The script should run the test and report
the result by one of:

  * for a test pass: exit with status 0
  * where a test has been skipped (perhaps because it depends on an
    external program which can't be found) execute: `test_skip "reason"'.
    (test_skip() is defined in common.sh; it exits with status 120)
  * for a test fail: exit with some status other than 0 or 120

The Suite is arranged as such:

setup-test
    Create the test framework.  This will re-generate your configure
    script and make files.

teardown-test
    Remove the temporary files created as part of the tests.

runtest
    Run a single test.

runalltests
    Run all tests in the suite

tests
    Directory containing the tests.  All files found in this and all
    sub-directories which have the name test-* will be treated as one
    test each.

Complex tests may be given their own directory as long as there is a file
named 'test-*' in the directory which will launch the test.
