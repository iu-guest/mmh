#!/bin/sh
#
# Tests to see if bursting messages works correctly.
#

. "$MH_TEST_COMMON"

#
# Create a test message that we can burst
#

msgpath="`mhpath b`"
msgnum="${msgpath##*/}"

cat > "$msgpath" <<EOF
From: Test Burst Message <burst1@example.com>
To: Test Recipient <recipient@example.com>
Date: Friday, 29 Sep 2006 00:00:00
Subject: Test digest

------- Message one

From: Mister Burster <burst2@example.com>
To: Nobody 1 <nobody1@example.com>
Date: Thursday, 28 Sep 2006 00:01:00
Subject: Message one

This is message one

------- Message two

From: Mister Burster <burst3@example.com>
To: Nobody 2 <nobody2@example.com>
Date: Thursday, 28 Sep 2006 00:02:00
Subject: Message two

This is message two
For real.

-------

End of all messages
EOF

runandcheck "scan $msgnum" <<!
  11  2006-09-29 00:00  Test Burst Messag  Test digest
!

runandcheck "burst $msgnum" <<!
!

runandcheck "scan $msgnum-l" <<!
  11  2006-09-29 00:00  Test Burst Messag  Test digest
  12+ 2006-09-28 00:01  Mister Burster     Message one
  13  2006-09-28 00:02  Mister Burster     Message two
!


#
# Create another test message that stresses the boundary checker a bit
#

msgpath="`mhpath b`"
msgnum="${msgpath##*/}"

cat >"$msgpath" <<EOF
From: Test Burst Message <burst1@example.com>
To: Test Recipient <recipient@example.com>
Date: Friday, 29 Sep 2006 00:00:00
Subject: Test digest

This is a preamble
------- Message one

From: Mister Burster <burst2@example.com>
To: Nobody 1 <nobody1@example.com>
Date: Thursday, 28 Sep 2006 00:01:00
Subject: Message one

- -This is message one

------- Message two
From: Mister Burster <burst3@example.com>
To: Nobody 2 <nobody2@example.com>
Date: Thursday, 28 Sep 2006 00:02:00
Subject: Message two

This is message two
- For real.
------- Message three



From: Mister Burster <burst4@example.com>
To: Nobody 3 <nobody3@example.com>
Date: Thursday, 28 Sep 2006 00:03:00
Subject: Message three

Will this one work?


------ Message three
End of all messages
EOF

runandcheck "burst $msgnum" <<!
burst: message 14 not in digest format, continuing...
!

# mmh's burst(1) is picky about the input format

exit

# If we make burst to be more liberal in accepting not perfect
# boundaries in the input, we can run the following tests


runandcheck "scan $msgnum-l" <<!
  14  09/29 Test Burst Messag  Test digest
  15+ 09/28 Mister Burster     Message one
  16  09/28 Mister Burster     Message two
  17  09/28 Mister Burster     Message three
!

#
# Check to see if each message is what we expect
#

folder -fast "$msgnum" >/dev/null

runandcheck "next" <<!
From: Mister Burster <burst2@example.com>
To: Nobody 1 <nobody1@example.com>
Date: Thursday, 28 Sep 2006 00:01:00
Subject: Message one

-This is message one
!

runandcheck "next" <<!
From: Mister Burster <burst3@example.com>
To: Nobody 2 <nobody2@example.com>
Date: Thursday, 28 Sep 2006 00:02:00
Subject: Message two

This is message two
For real.
!

runandcheck "next" <<!
From: Mister Burster <burst4@example.com>
To: Nobody 3 <nobody3@example.com>
Date: Thursday, 28 Sep 2006 00:03:00
Subject: Message three

Will this one work?

!
