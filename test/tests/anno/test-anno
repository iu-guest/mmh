#!/bin/sh
######################################################
#
# Test anno
#
######################################################

. "$MH_TEST_COMMON"


# check -help
runandcheck "anno -help" <<!
Usage: anno [+folder] [msgs] [switches]
  switches are:
  -component field
  -[no]date
  -text body
  -Version
  -help
  -list
  -delete
  -number
  -append
  -[no]preserve
!


# check -Version
case `anno -V` in
  anno\ --*) ;;
  *        ) printf '%s: anno -V generated unexpected output\n' "$0" >&2
             failed=`expr "${failed:-0}" + 1`;;
esac


# check unknown switch
runandcheck "anno -nonexistent ||:" <<!
anno: -nonexistent unknown
!


cp "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"

# check with no switches

runandcheck 'anno -comp Nmh-test 11' <<!
!
sed -e 's/^Nmh-test:.*/Nmh-test:/' "`mhpath 11`" >"`mhpath b`"
runandcheck "cat `mhpath l`" <<!
Nmh-test:
`cat "${MH_TEST_DIR}/Mail/inbox/1"`
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"


# check -component

runandcheck 'anno -component Nmh-test 11' <<!
!
sed -e 's/^Nmh-test:.*/Nmh-test:/' "`mhpath 11`" >"`mhpath b`"
runandcheck "cat `mhpath l`" <<!
Nmh-test:
`cat "${MH_TEST_DIR}/Mail/inbox/1"`
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"


# check +folder

runandcheck 'anno -component Nmh-test +inbox 11' <<!
!
sed -e 's/^Nmh-test:.*/Nmh-test:/' "`mhpath 11`" >"`mhpath b`"
runandcheck "cat `mhpath l`" <<!
Nmh-test:
`cat "${MH_TEST_DIR}/Mail/inbox/1"`
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"




# check msg

folder -fast 2 >/dev/null

runandcheck "anno 11 -component Nmh-test" <<!
!
sed -e 's/^Nmh-test:.*/Nmh-test:/' "`mhpath 11`" >"`mhpath b`"
runandcheck "cat `mhpath l`" <<!
Nmh-test:
`cat "${MH_TEST_DIR}/Mail/inbox/1"`
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"



# Hard link the message and verify that the new one does get annotated.

ln "${MH_TEST_DIR}/Mail/inbox/8" "${MH_TEST_DIR}/Mail/inbox/8.link"
runandcheck "anno 8 -component Nmh-test" <<!
!
runandcheck "diff -u $MH_TEST_DIR/Mail/inbox/8 $MH_TEST_DIR/Mail/inbox/8.link" <<!
!


# check -nodate.  Without -text, it doesn't change the message.

runandcheck "anno 11 -component Nmh-test -nodate" <<!
anno: -nodate without -text is a no-op.
!
runandcheck "diff -u `mhpath f` `mhpath 11`" <<!
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"


# check -date

runandcheck "anno 11 -component Nmh-test -nodate -date" <<!
!
sed -e 's/^Nmh-test:.*/Nmh-test:/' "`mhpath 11`" >"`mhpath b`"
runandcheck "cat `mhpath l`" <<!
Nmh-test:
`cat "${MH_TEST_DIR}/Mail/inbox/1"`
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"



# check -append

runandcheck "anno 11 -component Nmh-test -append" <<!
!
sed -e 's/^Nmh-test:.*/Nmh-test:/' "`mhpath 11`" >"`mhpath b`"
runandcheck "cat `mhpath l`" <<!
`awk '/^$/{print "Nmh-test:"}1' "${MH_TEST_DIR}/Mail/inbox/1"`
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"


# check -list

runandcheck "anno 11 -component Nmh-test -nodate -text 'test of anno -list'" <<!
!
runandcheck "cat `mhpath 11`" <<!
Nmh-test: test of anno -list
`cat "${MH_TEST_DIR}/Mail/inbox/1"`
!
runandcheck 'anno 11 -component Nmh-test -list' <<!
test of anno -list
!
# check -list -number
runandcheck 'anno 11 -component Nmh-test -list -number' <<!
1	test of anno -list
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"


# check -delete

runandcheck "anno 11 -component Nmh-test" <<!
!
runandcheck "anno 11 -component Nmh-test -delete" <<!
!
runandcheck "cat `mhpath 11`" <<!
`cat "$MH_TEST_DIR/Mail/inbox/1"`
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"


# check -delete -number
anno 11 -component Nmh-test -nodate -text 3
anno 11 -component Nmh-test -nodate -text 2
anno 11 -component Nmh-test -nodate -text 1
runandcheck "anno 11 -component Nmh-test -delete -number 2" <<!
!
runandcheck "cat `mhpath 11`" <<!
Nmh-test: 1
Nmh-test: 3
`cat "$MH_TEST_DIR/Mail/inbox/1"`
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"


# check -delete -all
anno 11 -component Nmh-test -nodate -text 3
anno 11 -component Nmh-test -nodate -text 2
anno 11 -component Nmh-test -nodate -text 1
runandcheck "anno 11 -component Nmh-test -delete -number all" <<!
!
runandcheck "cat `mhpath 11`" <<!
`cat "$MH_TEST_DIR/Mail/inbox/1"`
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"


# check -preserve

touch -t '201210010000.00' "${MH_TEST_DIR}/Mail/inbox/11"
output="`ls -l \`mhpath 11\``"
runandcheck "anno 11 -component Nmh-test -preserve" <<!
!
runandcheck "anno 11 -component Nmh-test -preserve -delete" <<!
!
runandcheck "ls -l `mhpath 11`" <<!
$output
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"


# check -nopreserve

touch -t '201210010000.00' "${MH_TEST_DIR}/Mail/inbox/11"
out1="`ls -l \`mhpath 11\``"
runandcheck "anno 11 -component Nmh-test -preserve -nopreserve" <<!
!
out2="`ls -l \`mhpath 11\``"
runandcheck "test \"$out1\" = \"$out2\" || echo differs" <<!
differs
!

touch -t '201210010000.00' "${MH_TEST_DIR}/Mail/inbox/11"
out1="`ls -l \`mhpath 11\``"
runandcheck "anno 11 -component Nmh-test -preserve -nopreserve -delete" <<!
!
out2="`ls -l \`mhpath 11\``"
runandcheck "test \"$out1\" = \"$out2\" || echo differs" <<!
differs
!
cp -f "${MH_TEST_DIR}/Mail/inbox/1" "${MH_TEST_DIR}/Mail/inbox/11"

