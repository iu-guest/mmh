/*
** utils.h -- utility prototypes
*/

void *mh_xrealloc(void *, size_t);
void *mh_xcalloc(size_t, size_t);
void mh_free0(void *);
char *pwd(void);
char *add(char *, char *);
void create_folder(char *, int, void (*)(int));
int num_digits(int);
char *mh_xstrdup(char *);

struct msgs_array {
	int max, size;
	char **msgs;
};

void app_msgarg(struct msgs_array *, char *);
